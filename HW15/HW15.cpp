﻿#include <iostream>

void AllEvenNumbers(int a, int N)
{
    std::cout << "All EVen Numbers" << "\n";

    while (a != N)
    {
        std::cout << a + 2 << "\n";
        a = a + 2;
    }
}

int main()
{
    const int N = 18;
    static int a = 0;
    AllEvenNumbers(a, N);

    std::cout << "\n" << "All Odd Numbers" << "\n";
    for (int i = 1; i <= N; i = i + 2)
        std::cout << i << "\n";
}